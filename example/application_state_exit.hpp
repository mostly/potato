#ifndef APPLICATION_STATE_EXIT_HPP
#define APPLICATION_STATE_EXIT_HPP

#include "../potato/potato.hpp"

#include "application_states.hpp"

class ApplicationStateExit : public potato::State
{
    public:
    	void        handle_events();
		std::string logic();
    	void        render(SDL_Renderer* screenRenderer);
		void        reset();
};

#endif