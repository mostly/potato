#ifndef APPLICATION_STATE_01_HPP
#define APPLICATION_STATE_01_HPP

#include <string>

#include "../potato/potato.hpp"
#include "application_states.hpp"

class ApplicationState01 : public potato::State
{
	private:
		bool             m_b_successfulInit;
		bool             m_b_firstTimeThrough;
		potato::Texture* m_p_previewImage;
		std::string      m_s_initMessage;

    public:
		ApplicationState01(SDL_Renderer* screenRenderer);
    	~ApplicationState01();
		std::string get_init_message();
		bool        get_successful_init_flag();
		void        handle_events();
		std::string logic();
		bool        init(SDL_Renderer* screenRenderer);
    	void        render(SDL_Renderer* screenRenderer);
		void        reset();
};

#endif