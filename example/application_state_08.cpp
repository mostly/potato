#include "application_state_08.hpp"

ApplicationState08::ApplicationState08(SDL_Renderer* screenRenderer)
{
    m_b_successfulInit = init(screenRenderer);
}

ApplicationState08::~ApplicationState08()
{
    delete m_p_fadeOutImage;
	delete m_p_fadeInImage;
}

bool ApplicationState08::get_successful_init_flag()
{
	return m_b_successfulInit;
}

std::string ApplicationState08::get_init_message()
{
	return m_s_initMessage;
}

void ApplicationState08::handle_events()
{
	SDL_Event eventQueue; 
	// Poll the event queue
	while (SDL_PollEvent(&eventQueue) != 0)
	{
		switch (eventQueue.type)
		{
			case SDL_QUIT: // User requests quit
				m_b_quitPressed = true;
				break;
			case SDL_KEYDOWN: // User presses a key
				switch (eventQueue.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						m_b_quitPressed = true;
						break;

                    case SDLK_w: // Increase alpha
						if (m_u8_alpha + 32 > 255)
						{
							m_u8_alpha = 255;
						}
						else
						{
							m_u8_alpha += 32;
						}
						break;
                        
                    case SDLK_s: // Decrease alpha 
						if (m_u8_alpha - 32 < 0)
						{
							m_u8_alpha = 0;
						}
						else
						{
							m_u8_alpha -= 32;
						}
						break;

					default:
						break;
				}
				break;
			default:
				break;
		}
	}
}

bool ApplicationState08::init(SDL_Renderer* screenRenderer)
{
	const std::string cs_fadeOutImagePath = "../example/resources/images/development/fadeout.png";
	m_p_fadeOutImage = new potato::Texture();
	if (!(m_p_fadeOutImage->load_from_file(cs_fadeOutImagePath.c_str(), screenRenderer)))
	{
		m_s_initMessage = m_p_fadeOutImage->get_error_message();
		return false;
	}
	m_p_fadeOutImage->set_blend_mode(SDL_BLENDMODE_BLEND);

	const std::string cs_fadeInImagePath = "../example/resources/images/development/fadein.png";
	m_p_fadeInImage = new potato::Texture();
	if (!(m_p_fadeInImage->load_from_file(cs_fadeInImagePath.c_str(), screenRenderer)))
	{
		m_s_initMessage = m_p_fadeInImage->get_error_message();
		return false;
	}

	m_b_quitPressed = false;

	m_u8_alpha = 255;

	return true;
}

std::string ApplicationState08::logic()
{
	if (m_b_quitPressed)
	{
		return STATE_09;	
	}
	else
	{
		return STATE_NULL;
	}
	
}

void ApplicationState08::render(SDL_Renderer* screenRenderer)
{
	//Render background
	m_p_fadeInImage->render(0, 0, screenRenderer);

	//Render front blended
	m_p_fadeOutImage->set_alpha(m_u8_alpha);
	m_p_fadeOutImage->render(0, 0, screenRenderer);
}

void ApplicationState08::reset()
{

}
