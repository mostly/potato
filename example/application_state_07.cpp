#include "application_state_07.hpp"

ApplicationState07::ApplicationState07(SDL_Renderer* screenRenderer)
{
    m_b_successfulInit = init(screenRenderer);
}

ApplicationState07::~ApplicationState07()
{
    delete m_p_previewImage;
}

std::string ApplicationState07::get_init_message()
{
	return m_s_initMessage;
}

bool ApplicationState07::get_successful_init_flag()
{
	return m_b_successfulInit;
}

bool ApplicationState07::init(SDL_Renderer* screenRenderer)
{
	const std::string cs_testImagePath = "../example/resources/images/development/colors.png";
	m_p_previewImage = new potato::Texture();
	if (!(m_p_previewImage->load_from_file(cs_testImagePath.c_str(), screenRenderer)))
	{
		m_s_initMessage = m_p_previewImage->get_error_message();
		return false;
	}

	m_b_quitPressed = false;
	m_u8_red = 255;
	m_u8_green = 255;
    m_u8_blue = 255;

	return true;
}

void ApplicationState07::handle_events()
{
	SDL_Event eventQueue; 
	// Poll the event queue
	while (SDL_PollEvent(&eventQueue) != 0)
	{
		switch (eventQueue.type)
		{
			case SDL_QUIT: // User requests quit
				m_b_quitPressed = true;
				break;
			case SDL_KEYDOWN: // User presses a key
				switch (eventQueue.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						m_b_quitPressed = true;
						break;
					
					//Increase red
					case SDLK_q:
					m_u8_red += 32;
					break;
					
					//Increase green
					case SDLK_w:
					m_u8_green += 32;
					break;
					
					//Increase blue
					case SDLK_e:
					m_u8_blue += 32;
					break;
					
					//Decrease red
					case SDLK_a:
					m_u8_red -= 32;
					break;
					
					//Decrease green
					case SDLK_s:
					m_u8_green -= 32;
					break;
					
					//Decrease blue
					case SDLK_d:
					m_u8_blue -= 32;
					break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
}

std::string ApplicationState07::logic()
{
	if (m_b_quitPressed)
	{
		return STATE_08;	
	}
	else
	{
		return STATE_NULL;
	}
	
}

void ApplicationState07::render(SDL_Renderer* screenRenderer)
{
	// Render texture to screen
	m_p_previewImage->set_colour(m_u8_red, m_u8_green, m_u8_blue);
	m_p_previewImage->render_stretched(0, 0, screenRenderer);
}

void ApplicationState07::reset()
{

}
