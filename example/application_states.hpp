#ifndef APPLICATION_STATES_HPP
#define APPLICATION_STATES_HPP

// SDL Delay
#define STATE_01 "STATE_01"

// SDL Keypress 
#define STATE_02 "STATE_02"

// SDL Geometry Render
#define STATE_03 "STATE_03"

// SDL Viewports
#define STATE_04 "STATE_04"

// SDL Colour Keying
#define STATE_05 "STATE_05"

// SDL Clipsheets
#define STATE_06 "STATE_06"

// SDL Colour Modulation
#define STATE_07 "STATE_07"

// SDL Alpha Blending
#define STATE_08 "STATE_08"

// SDL Animation
#define STATE_09 "STATE_09"

#define STATE_EXIT "STATE_EXIT"
#define STATE_NULL "STATE_NULL"

#endif