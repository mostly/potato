#ifndef APPLICATION_STATE_06_HPP
#define APPLICATION_STATE_06_HPP

#include "../potato/potato.hpp"

#include "application_states.hpp"

class ApplicationState06 : public potato::State
{
	private:
		bool               m_b_successfulInit;
		bool               m_b_quitPressed;
		int                m_i_screenWidth;
		int                m_i_screenHeight;
		potato::Clipsheet* m_p_clipsheet;
		potato::Texture*   m_p_testImage;
		std::string        m_s_initMessage;

    public:
		ApplicationState06(SDL_Renderer* screenRenderer, int screenWidth, 
		                   int screenHeight);
    	~ApplicationState06();
		std::string get_init_message();
		bool        get_successful_init_flag();
		void        handle_events();
		bool        init(SDL_Renderer* screenRenderer, int screenWidth, 
		                 int screenHeight);
		std::string logic();
		void        render(SDL_Renderer* screenRenderer);
		void        reset();
};

#endif