#include "application_state_05.hpp"

ApplicationState05::ApplicationState05(SDL_Renderer* screenRenderer)
{
    m_b_successfulInit = init(screenRenderer);
}

ApplicationState05::~ApplicationState05()
{
    delete m_p_backgroundImage;
	delete m_p_characterImage;
}

bool ApplicationState05::get_successful_init_flag()
{
	return m_b_successfulInit;
}

std::string ApplicationState05::get_init_message()
{
	return m_s_initMessage;
}

void ApplicationState05::handle_events()
{
	SDL_Event eventQueue; 
	// Poll the event queue
	while (SDL_PollEvent(&eventQueue) != 0)
	{
		switch (eventQueue.type)
		{
			case SDL_QUIT: // User requests quit
				m_b_quitPressed = true;
				break;
			case SDL_KEYDOWN: // User presses a key
				switch (eventQueue.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						m_b_quitPressed = true;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
}

bool ApplicationState05::init(SDL_Renderer* screenRenderer)
{
	const std::string cs_backgroundImagePath = "../example/resources/images/development/background.png";
	m_p_backgroundImage = new potato::Texture();
	if (!(m_p_backgroundImage->load_from_file(cs_backgroundImagePath.c_str(), screenRenderer)))
	{
		m_s_initMessage = m_p_backgroundImage->get_error_message();
		return false;
	}

	const std::string cs_characterImagePath = "../example/resources/images/development/foo.png";
	m_p_characterImage = new potato::Texture();
	if (!(m_p_characterImage->load_from_file(cs_characterImagePath.c_str(), screenRenderer)))
	{
		m_s_initMessage = m_p_characterImage->get_error_message();
		return false;
	}

	m_b_quitPressed = false;

	return true;
}

std::string ApplicationState05::logic()
{
	if (m_b_quitPressed)
	{
		return STATE_06;	
	}
	else
	{
		return STATE_NULL;
	}
	
}

void ApplicationState05::render(SDL_Renderer* screenRenderer)
{
	// Render texture to screen
	m_p_backgroundImage->render_stretched(0, 0, screenRenderer);
	m_p_characterImage->render(240, 190, screenRenderer);
}

void ApplicationState05::reset()
{

}
