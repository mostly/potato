#include "application_state_09.hpp"

ApplicationState09::ApplicationState09(SDL_Renderer* screenRenderer, int screenWidth, int screenHeight)
{
	m_p_clipsheet = nullptr;
    m_b_successfulInit = init(screenRenderer, screenWidth, screenHeight);
}

ApplicationState09::~ApplicationState09()
{
	if (m_p_clipsheet != nullptr)
	{
		delete m_p_clipsheet;
	}

	if (m_p_characterImage != nullptr)
	{
    	delete m_p_characterImage;
	}
}

std::string ApplicationState09::get_init_message()
{
	return m_s_initMessage;
}

bool ApplicationState09::get_successful_init_flag()
{
	return m_b_successfulInit;
}

void ApplicationState09::handle_events()
{
	SDL_Event eventQueue; 
	// Poll the event queue
	while (SDL_PollEvent(&eventQueue) != 0)
	{
		switch (eventQueue.type)
		{
			case SDL_QUIT: // User requests quit
				m_b_quitPressed = true;
				break;
			case SDL_KEYDOWN: // User presses a key
				switch (eventQueue.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						m_b_quitPressed = true;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
}

bool ApplicationState09::init(SDL_Renderer* screenRenderer, int screenWidth, int screenHeight)
{
	m_b_quitPressed = false;

	m_i_frame = 0;
	
	m_i_screenWidth = screenWidth;
	
	m_i_screenHeight = screenHeight;

	potato::ConfigFile configFile("../example/resources/config/development/foo.xml");
	m_p_clipsheet = configFile.get_clipsheet();
	m_p_characterImage = configFile.get_texture(screenRenderer);
	if (configFile.in_error_state())
	{
		m_s_initMessage = configFile.get_error_message();
		return false;
	}
	
	return true;
}

std::string ApplicationState09::logic()
{
	if (m_b_quitPressed)
	{
		return STATE_EXIT;	
	}
	else
	{
		m_i_frame++;
		return STATE_NULL;
	}	
}

void ApplicationState09::render(SDL_Renderer* screenRenderer)
{
	SDL_Rect* currentClip = nullptr;
	switch (m_i_frame)
	{
		case 1:
			currentClip = m_p_clipsheet->get("WalkFrame2");
			break;
		case 2:
			currentClip = m_p_clipsheet->get("WalkFrame3");
			break;
		case 3:
			currentClip = m_p_clipsheet->get("WalkFrame4");
			break;
		default:
			m_i_frame = 0;
			currentClip = m_p_clipsheet->get("WalkFrame1");
			break;
	}
	m_p_characterImage->render((m_i_screenWidth - currentClip->w ) / 2,
	                           (m_i_screenHeight - currentClip->h) / 2,
							   screenRenderer, currentClip);
}

void ApplicationState09::reset()
{

}
