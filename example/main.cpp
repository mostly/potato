#include "example_application.hpp"

int main(int argc, char* args[])
{
	#if LOGGING
	SET_LOG_THREAD_NAME("MAIN");
	#endif

	Application* myApp = new Application(); 

	const int init_result = myApp->initialise(); 
    if (init_result < 0)
    {
        return init_result;
    }

	#if LOGGING
	SET_LOG_THREAD_NAME("MAIN");
	#endif

	myApp->main_loop();

    myApp->clean_up();

    return 0;
}
