#ifndef APPLICATION_STATE_09_HPP
#define APPLICATION_STATE_09_HPP

#include "../potato/potato.hpp"

#include "application_states.hpp"

class ApplicationState09 : public potato::State
{
	private:
		bool               m_b_successfulInit;
		bool               m_b_quitPressed;
        int                m_i_frame;
		int                m_i_screenWidth;
		int                m_i_screenHeight;
		potato::Clipsheet* m_p_clipsheet;
		potato::Texture*   m_p_characterImage;
		std::string        m_s_initMessage;

    public:
		ApplicationState09(SDL_Renderer* screenRenderer, int screenWidth, 
		                   int screenHeight);
    	~ApplicationState09();
		std::string get_init_message();
		bool        get_successful_init_flag();
		void        handle_events();
		bool        init(SDL_Renderer* screenRenderer, int screenWidth, 
		                 int screenHeight);
		std::string logic();
		void        render(SDL_Renderer* screenRenderer);
		void        reset();
};

#endif