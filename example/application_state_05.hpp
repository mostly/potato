#ifndef APPLICATION_STATE_05_HPP
#define APPLICATION_STATE_05_HPP

#include "../potato/potato.hpp"

#include "application_states.hpp"

class ApplicationState05 : public potato::State
{
	private:
		bool             m_b_successfulInit;
		bool             m_b_quitPressed;
		potato::Texture* m_p_backgroundImage;
		potato::Texture* m_p_characterImage;
		std::string      m_s_initMessage;

    public:
		ApplicationState05(SDL_Renderer* screenRenderer);
    	~ApplicationState05();
		std::string get_init_message();
		bool        get_successful_init_flag();
		void        handle_events();
		bool        init(SDL_Renderer* screenRenderer);
		std::string logic();
		void        render(SDL_Renderer* screenRenderer);
		void        reset();
};

#endif