#ifndef APPLICATION_STATE_03_HPP
#define APPLICATION_STATE_03_HPP

#include "../potato/potato.hpp"

#include "application_states.hpp"

class ApplicationState03 : public potato::State
{
	private:
		bool        m_b_successfulInit;
		bool        m_b_quitPressed;
		int         m_i_screenWidth;
		int         m_i_screenHeight;
		std::string m_s_initMessage;

    public:
		ApplicationState03(int screenWidth, int screenHeight);
    	~ApplicationState03();
		std::string get_init_message();
		bool        get_successful_init_flag();
		void        handle_events();
		bool        init(int screenWidth, int screenHeight);
		std::string logic();
		void        render(SDL_Renderer* screenRenderer);
		void        reset();
};

#endif