#ifndef APPLICATION_STATE_04_HPP
#define APPLICATION_STATE_04_HPP

#include "../potato/potato.hpp"

#include "application_states.hpp"

class ApplicationState04 : public potato::State
{
	private:
		bool             m_b_successfulInit;
		bool             m_b_quitPressed;
		int              m_i_screenWidth;
		int              m_i_screenHeight;
		SDL_Rect         m_o_topLeftViewport;
		SDL_Rect         m_o_topRightViewport;
		SDL_Rect         m_o_bottomViewport;
		SDL_Rect         m_o_fullViewport;
		potato::Texture* m_p_previewImage;
		std::string      m_s_initMessage;
    public:
		ApplicationState04(SDL_Renderer* screenRenderer, int screenWidth, 
		                   int screenHeight);
    	~ApplicationState04();
		std::string get_init_message();
		bool        get_successful_init_flag();
		void        handle_events();
		bool        init(SDL_Renderer* screenRenderer, int screenWidth, 
		                 int screenHeight);
		std::string logic();
    	void        render(SDL_Renderer* screenRenderer);
		void        reset();
};

#endif