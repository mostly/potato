#include "application_state_02.hpp"

ApplicationState02::ApplicationState02(SDL_Renderer* screenRenderer)
{
    m_b_successfulInit = init(screenRenderer);
}

ApplicationState02::~ApplicationState02()
{
    delete m_p_previewImage;
}

std::string ApplicationState02::get_init_message()
{
	return m_s_initMessage;
}

bool ApplicationState02::get_successful_init_flag()
{
	return m_b_successfulInit;
}

bool ApplicationState02::init(SDL_Renderer* screenRenderer)
{
	const std::string cs_testImagePath = "../example/resources/images/development/preview.png";
	m_p_previewImage = new potato::Texture();
	if (!(m_p_previewImage->load_from_file(cs_testImagePath.c_str(), screenRenderer)))
	{
		m_s_initMessage = m_p_previewImage->get_error_message();
		return false;
	}

	m_b_quitPressed = false;

	return true;
}

void ApplicationState02::handle_events()
{
	SDL_Event eventQueue; 
	// Poll the event queue
	while (SDL_PollEvent(&eventQueue) != 0)
	{
		switch (eventQueue.type)
		{
			case SDL_QUIT: // User requests quit
				m_b_quitPressed = true;
				break;
			case SDL_KEYDOWN: // User presses a key
				switch (eventQueue.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						m_b_quitPressed = true;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
}

std::string ApplicationState02::logic()
{
	if (m_b_quitPressed)
	{
		return STATE_03;	
	}
	else
	{
		return STATE_NULL;
	}
	
}

void ApplicationState02::render(SDL_Renderer* screenRenderer)
{
	// Render texture to screen
	m_p_previewImage->render_stretched(0, 0, screenRenderer);
}

void ApplicationState02::reset()
{

}
