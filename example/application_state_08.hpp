#ifndef APPLICATION_STATE_08_HPP
#define APPLICATION_STATE_08_HPP

#include "../potato/potato.hpp"

#include "application_states.hpp"

class ApplicationState08 : public potato::State
{
	private:
		bool             m_b_successfulInit;
		bool             m_b_quitPressed;
		potato::Texture* m_p_fadeOutImage;
		potato::Texture* m_p_fadeInImage;
		std::string      m_s_initMessage;
		Uint8            m_u8_alpha;

    public:
		ApplicationState08(SDL_Renderer* screenRenderer);
    	~ApplicationState08();
		std::string get_init_message();
		bool        get_successful_init_flag();
		void        handle_events();
		bool        init(SDL_Renderer* screenRenderer);
		std::string logic();
		void        render(SDL_Renderer* screenRenderer);
		void        reset();
};

#endif