#ifndef APPLICATION_STATE_07_HPP
#define APPLICATION_STATE_07_HPP

#include "../potato/potato.hpp"

#include "application_states.hpp"

class ApplicationState07 : public potato::State
{
	private:
		bool             m_b_quitPressed;
		bool             m_b_successfulInit;
		potato::Texture* m_p_previewImage;
		std::string      m_s_initMessage;
		Uint8            m_u8_red;
		Uint8            m_u8_green;
		Uint8            m_u8_blue;

    public:
		ApplicationState07(SDL_Renderer* screenRenderer);
    	~ApplicationState07();
		std::string get_init_message();
		bool        get_successful_init_flag();
		void        handle_events();
		std::string logic();
		bool        init(SDL_Renderer* screenRenderer);
    	void        render(SDL_Renderer* screenRenderer);
		void        reset();
};

#endif