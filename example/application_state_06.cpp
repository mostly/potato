#include "application_state_06.hpp"

ApplicationState06::ApplicationState06(SDL_Renderer* screenRenderer, int screenWidth, int screenHeight)
{
	m_p_clipsheet = nullptr;
    m_b_successfulInit = init(screenRenderer, screenWidth, screenHeight);
}

ApplicationState06::~ApplicationState06()
{
	if (m_p_clipsheet != nullptr)
	{
		delete m_p_clipsheet;
	}

	if (m_p_testImage != nullptr)
	{
    	delete m_p_testImage;
	}
}

std::string ApplicationState06::get_init_message()
{
	return m_s_initMessage;
}

bool ApplicationState06::get_successful_init_flag()
{
	return m_b_successfulInit;
}

void ApplicationState06::handle_events()
{
	SDL_Event eventQueue; 
	// Poll the event queue
	while (SDL_PollEvent(&eventQueue) != 0)
	{
		switch (eventQueue.type)
		{
			case SDL_QUIT: // User requests quit
				m_b_quitPressed = true;
				break;
			case SDL_KEYDOWN: // User presses a key
				switch (eventQueue.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						m_b_quitPressed = true;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
}

bool ApplicationState06::init(SDL_Renderer* screenRenderer, int screenWidth, int screenHeight)
{
	const std::string cs_testImagePath = "../example/resources/images/development/dots.png";
	m_p_testImage = new potato::Texture();
	if (!(m_p_testImage->load_from_file(cs_testImagePath.c_str(), screenRenderer)))
	{
		m_s_initMessage = m_p_testImage->get_error_message();
		return false;
	}

	m_b_quitPressed = false;
	
	m_i_screenWidth = screenWidth;
	
	m_i_screenHeight = screenHeight;

	// Load the clipsheet from xml
	tinyxml2::XMLDocument xmlDoc;
	const std::string cs_dotsConfigPath = "../example/resources/config/development/dots.xml";
	tinyxml2::XMLError result = xmlDoc.LoadFile(cs_dotsConfigPath.c_str()); 
	if (result != tinyxml2::XML_SUCCESS)
	{
		m_s_initMessage = "Failed to load config [" + cs_dotsConfigPath + "]." 
		                  + "Error = " + std::to_string(result);
		return false;
	}
	
	tinyxml2::XMLNode * pRoot = xmlDoc.FirstChild();
	if (pRoot == nullptr)
	{
		m_s_initMessage = "No root node found in config [" + cs_dotsConfigPath + "].";
		return false;
	}
	
	tinyxml2::XMLElement* pElement = pRoot->FirstChildElement("Clipsheet");
	m_p_clipsheet = new potato::Clipsheet(pElement);
	if (!(m_p_clipsheet->get_successful_init_flag()))
	{
		m_s_initMessage = m_p_clipsheet->get_init_message();
		return false;	
	}
	
	return true;
}

std::string ApplicationState06::logic()
{
	if (m_b_quitPressed)
	{
		return STATE_07;	
	}
	else
	{
		return STATE_NULL;
	}
	
}

void ApplicationState06::render(SDL_Renderer* screenRenderer)
{
	//Render top left sprite
	m_p_testImage->render(0, 
	                      0, 
						  screenRenderer, 
						  m_p_clipsheet->get("TopLeft"));

	//Render top right sprite
	m_p_testImage->render(m_i_screenWidth - m_p_clipsheet->get("TopRight")->w, 
	                      0, 
						  screenRenderer, 
						  m_p_clipsheet->get("TopRight"));

	//Render bottom left sprite
	m_p_testImage->render(0, 
	                      m_i_screenHeight - m_p_clipsheet->get("BottomLeft")->h, 
						  screenRenderer, 
						  m_p_clipsheet->get("BottomLeft"));

	//Render bottom right sprite
	m_p_testImage->render(m_i_screenWidth - m_p_clipsheet->get("BottomRight")->w, 
	                      m_i_screenHeight - m_p_clipsheet->get("BottomRight")->h, 
						  screenRenderer, 
						  m_p_clipsheet->get("BottomRight"));
}

void ApplicationState06::reset()
{

}
