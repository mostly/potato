#include "example_application.hpp"

Application::Application()
{
	m_i_screenHeight   = 0;
	m_i_screenWidth    = 0;
	m_p_stateManager   = nullptr;
	m_p_window         = nullptr;
	m_p_windowRenderer = nullptr;
}

Application::~Application()
{
	// StateManager
	delete m_p_stateManager;

	// Clean up SDL
	SDL_DestroyRenderer(m_p_windowRenderer);
    SDL_DestroyWindow(m_p_window);

    SDL_Quit();
	IMG_Quit();
}

int Application::initialise()
{
	#if LOGGING
	SET_LOG_THREAD_NAME("APPLICATION_INITIALISE");
	#endif

	// Set up constants
	m_i_screenWidth = 640;
	m_i_screenHeight = 480;

	// Initialise SDL components
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
		#if LOGGING
        LOG_ERROR("SDL could not initialize! SDL_Error: %s\n", 
				  SDL_GetError());
		#endif
        return ERROR_SDL_INIT;
    }
	LOG_DEBUG("Initialized SDL.");

	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		#if LOGGING
		LOG_ERROR("SDL_image could not initialize! SDL_image Error: %s\n", 
		          IMG_GetError());
		#endif
		return ERROR_IMG_INIT;
	}
	LOG_DEBUG("Initialized SDL_image.");

	// Prepare the window
	m_p_window = SDL_CreateWindow("BCR", SDL_WINDOWPOS_UNDEFINED, 
				 SDL_WINDOWPOS_UNDEFINED, m_i_screenWidth, m_i_screenHeight, 
				 SDL_WINDOW_SHOWN);
	if (m_p_window == nullptr)
	{
		#if LOGGING
		LOG_ERROR("Window could not be created! SDL_Error: %s\n", 
				  SDL_GetError());
		#endif
		return ERROR_WINDOW_INIT;
	}

	// Set texture filtering to linear
	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
	{
		LOG_WARNING("Linear texture filtering not enabled!");
	}

	// Create renderer for window
	m_p_windowRenderer = SDL_CreateRenderer(m_p_window, -1, 
	                     SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (m_p_windowRenderer == nullptr)
	{
		#if LOGGING
		LOG_ERROR("Renderer could not be created! SDL Error: %s\n", 
		          SDL_GetError());
		#endif
		return ERROR_RENDERER_INIT;
	}
	// Initialize renderer color
	SDL_SetRenderDrawColor(m_p_windowRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

	// Set up State Manager
	m_p_stateManager = new potato::StateManager();
	LOG_DEBUG("Initialized StateManager.");
	
	// STATE_01
    ApplicationState01* p_state01 = new ApplicationState01(m_p_windowRenderer);
	if (!(m_p_stateManager->register_state(STATE_01, p_state01)))
	{
		#if LOGGING
		LOG_ERROR("Unable to register STATE_01 with state manager.");
		#endif
		return ERROR_INIT_STATE_01;
	}
	if (!(p_state01->get_successful_init_flag()))
	{
		#if LOGGING
		LOG_ERROR(p_state01->get_init_message());
		#endif
		return ERROR_INIT_STATE_01;
	}
	LOG_DEBUG("Added STATE_01");

	// STATE_02
    ApplicationState02* p_state02 = new ApplicationState02(m_p_windowRenderer);
	if (!(m_p_stateManager->register_state(STATE_02, p_state02)))
	{
		#if LOGGING
		LOG_ERROR("Unable to register STATE_02 with state manager.");
		#endif
		return ERROR_INIT_STATE_02;
	}
	if (!(p_state02->get_successful_init_flag()))
	{
		#if LOGGING
		LOG_ERROR(p_state02->get_init_message());
		#endif
		return ERROR_INIT_STATE_02;
	}
	LOG_DEBUG("Added STATE_02");

	// STATE_03
    ApplicationState03* p_state03 = new ApplicationState03(m_i_screenWidth, 
	                                                       m_i_screenHeight);
	if (!(m_p_stateManager->register_state(STATE_03, p_state03)))
	{
		#if LOGGING
		LOG_ERROR("Unable to register STATE_03 with state manager.");
		#endif
		return ERROR_INIT_STATE_03;
	}
	if (!(p_state03->get_successful_init_flag()))
	{
		#if LOGGING
		LOG_ERROR(p_state03->get_init_message());
		#endif
		return ERROR_INIT_STATE_03;
	}
	LOG_DEBUG("Added STATE_03");

	// STATE_04
    ApplicationState04* p_state04 = new ApplicationState04(m_p_windowRenderer,
		                                                   m_i_screenWidth, 
	                                                       m_i_screenHeight);
	if (!(m_p_stateManager->register_state(STATE_04, p_state04)))
	{
		#if LOGGING
		LOG_ERROR("Unable to register STATE_04 with state manager.");
		#endif
		return ERROR_INIT_STATE_04;
	}
	if (!(p_state04->get_successful_init_flag()))
	{
		#if LOGGING
		LOG_ERROR(p_state04->get_init_message());
		#endif
		return ERROR_INIT_STATE_04;
	}
	LOG_DEBUG("Added STATE_04");

	// STATE_05
    ApplicationState05* p_state05 = new ApplicationState05(m_p_windowRenderer);
	if (!(m_p_stateManager->register_state(STATE_05, p_state05)))
	{
		#if LOGGING
		LOG_ERROR("Unable to register STATE_05 with state manager.");
		#endif
		return ERROR_INIT_STATE_05;
	}
	if (!(p_state05->get_successful_init_flag()))
	{
		#if LOGGING
		LOG_ERROR(p_state05->get_init_message());
		#endif
		return ERROR_INIT_STATE_05;
	}
	LOG_DEBUG("Added STATE_05");

	// STATE_06
    ApplicationState06* p_state06 = new ApplicationState06(m_p_windowRenderer,
	                                                       m_i_screenWidth, 
	                                                       m_i_screenHeight);
	if (!(m_p_stateManager->register_state(STATE_06, p_state06)))
	{
		#if LOGGING
		LOG_ERROR("Unable to register STATE_06 with state manager.");
		#endif
		return ERROR_INIT_STATE_06;
	}
	if (!(p_state06->get_successful_init_flag()))
	{
		#if LOGGING
		LOG_ERROR(p_state06->get_init_message());
		#endif
		return ERROR_INIT_STATE_06;
	}
	LOG_DEBUG("Added STATE_06");

	// STATE_07
    ApplicationState07* p_state07 = new ApplicationState07(m_p_windowRenderer);
	if (!(m_p_stateManager->register_state(STATE_07, p_state07)))
	{
		#if LOGGING
		LOG_ERROR("Unable to register STATE_07 with state manager.");
		#endif
		return ERROR_INIT_STATE_07;
	}
	if (!(p_state07->get_successful_init_flag()))
	{
		#if LOGGING
		LOG_ERROR(p_state07->get_init_message());
		#endif
		return ERROR_INIT_STATE_07;
	}
	LOG_DEBUG("Added STATE_07");

	// STATE_08
    ApplicationState08* p_state08 = new ApplicationState08(m_p_windowRenderer);
	if (!(m_p_stateManager->register_state(STATE_08, p_state08)))
	{
		#if LOGGING
		LOG_ERROR("Unable to register STATE_08 with state manager.");
		#endif
		return ERROR_INIT_STATE_08;
	}
	if (!(p_state08->get_successful_init_flag()))
	{
		#if LOGGING
		LOG_ERROR(p_state08->get_init_message());
		#endif
		return ERROR_INIT_STATE_08;
	}
	LOG_DEBUG("Added STATE_08");

	// STATE_09
    ApplicationState09* p_state09 = new ApplicationState09(m_p_windowRenderer,
	                                                       m_i_screenWidth, 
	                                                       m_i_screenHeight);
	if (!(m_p_stateManager->register_state(STATE_09, p_state09)))
	{
		#if LOGGING
		LOG_ERROR("Unable to register STATE_09 with state manager.");
		#endif
		return ERROR_INIT_STATE_09;
	}
	if (!(p_state09->get_successful_init_flag()))
	{
		#if LOGGING
		LOG_ERROR(p_state09->get_init_message());
		#endif
		return ERROR_INIT_STATE_09;
	}
	LOG_DEBUG("Added STATE_09");

	// STATE_EXIT
	if (!(m_p_stateManager->register_state(STATE_EXIT, 
		new ApplicationStateExit())))
	{
		#if LOGGING
		LOG_ERROR("Unable to register STATE_EXIT with state manager.");
		#endif
		return ERROR_INIT_STATE_EXIT;
	}
	LOG_DEBUG("Added STATE_EXIT");

    return 1;
}

void Application::main_loop()
{
	m_p_stateManager->set_next_state(STATE_01);
	m_p_stateManager->change_state();
	std::string nextState;

	// While the user hasn't quit
    while (m_p_stateManager->get_current_state_id() != STATE_EXIT)
    {
        // Do state event handling
        m_p_stateManager->get_current_state()->handle_events();

        // Do state logic
        nextState = m_p_stateManager->get_current_state()->logic();

		// Set the next state
		m_p_stateManager->set_next_state(nextState);

        // Change state if needed
        m_p_stateManager->change_state();

		// Clear screen
		SDL_RenderClear(m_p_windowRenderer);

        // Do state rendering
        m_p_stateManager->get_current_state()->render(m_p_windowRenderer);

		//Update screen
		SDL_RenderPresent(m_p_windowRenderer);
    }
}

void Application::clean_up()
{
	delete this;
}
