#include "application_state_04.hpp"

ApplicationState04::ApplicationState04(SDL_Renderer* screenRenderer, int screenWidth, int screenHeight)
{
    m_b_successfulInit = init(screenRenderer, screenWidth, screenHeight);
}

ApplicationState04::~ApplicationState04()
{
	delete m_p_previewImage;
}

std::string ApplicationState04::get_init_message()
{
	return m_s_initMessage;
}

bool ApplicationState04::get_successful_init_flag()
{
	return m_b_successfulInit;
}

void ApplicationState04::handle_events()
{
	SDL_Event eventQueue; 
	// Poll the event queue
	while (SDL_PollEvent(&eventQueue) != 0)
	{
		switch (eventQueue.type)
		{
			case SDL_QUIT: // User requests quit
				m_b_quitPressed = true;
				break;
			case SDL_KEYDOWN: // User presses a key
				switch (eventQueue.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						m_b_quitPressed = true;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
}

bool ApplicationState04::init(SDL_Renderer* screenRenderer, int screenWidth, int screenHeight)
{
	const std::string cs_testImagePath = "../example/resources/images/development/preview.png";
	m_p_previewImage = new potato::Texture();
	if (!(m_p_previewImage->load_from_file(cs_testImagePath.c_str(), screenRenderer)))
	{
		m_s_initMessage = m_p_previewImage->get_error_message();
		return false;
	}

	m_b_quitPressed = false;
	
	m_i_screenWidth = screenWidth;
	
	m_i_screenHeight = screenHeight;
	
	// Top left corner viewport
	m_o_topLeftViewport.x = 0;
	m_o_topLeftViewport.y = 0;
	m_o_topLeftViewport.w = m_i_screenWidth / 2;
	m_o_topLeftViewport.h = m_i_screenHeight / 2;
	
	// Top right corner viewport
	m_o_topRightViewport.x = m_i_screenWidth / 2;
	m_o_topRightViewport.y = 0;
	m_o_topRightViewport.w = m_i_screenWidth / 2;
	m_o_topRightViewport.h = m_i_screenHeight / 2;
	
	// Bottom viewport
	m_o_bottomViewport.x = 0;
	m_o_bottomViewport.y = m_i_screenHeight / 2;
	m_o_bottomViewport.w = m_i_screenWidth;
	m_o_bottomViewport.h = m_i_screenHeight / 2;
	
	// Full screen viewport
	m_o_fullViewport.x = 0;
	m_o_fullViewport.y = 0;
	m_o_fullViewport.w = m_i_screenWidth;
	m_o_fullViewport.h = m_i_screenHeight;

	return true;
}

std::string ApplicationState04::logic()
{
	if (m_b_quitPressed)
	{
		return STATE_05;	
	}
	else
	{
		return STATE_NULL;
	}
	
}

void ApplicationState04::render(SDL_Renderer* screenRenderer)
{
	SDL_RenderSetViewport(screenRenderer, &m_o_topLeftViewport);
	m_p_previewImage->render_stretched(0, 0, screenRenderer);
	SDL_RenderSetViewport(screenRenderer, &m_o_topRightViewport);
	m_p_previewImage->render_stretched(0, 0, screenRenderer);
	SDL_RenderSetViewport(screenRenderer, &m_o_bottomViewport);
	m_p_previewImage->render_stretched(0, 0, screenRenderer);
	SDL_RenderSetViewport(screenRenderer, &m_o_fullViewport);
}

void ApplicationState04::reset()
{

}
