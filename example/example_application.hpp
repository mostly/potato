#ifndef BCR_HPP
#define BCR_HPP

#include "../potato/potato.hpp"

#include "logger.hpp"
#include "error_codes.hpp"
#include "application_states.hpp"
#include "application_state_01.hpp"
#include "application_state_02.hpp"
#include "application_state_03.hpp"
#include "application_state_04.hpp"
#include "application_state_05.hpp"
#include "application_state_06.hpp"
#include "application_state_07.hpp"
#include "application_state_08.hpp"
#include "application_state_09.hpp"
#include "application_state_exit.hpp"

class Application
{
	private:
		int                   m_i_screenHeight;
		int                   m_i_screenWidth;
		SDL_Window*           m_p_window;
		SDL_Renderer*         m_p_windowRenderer;
	    potato::StateManager* m_p_stateManager;

	public:
		Application();
		~Application();
		void clean_up();
		int  initialise();
		void main_loop();
};

#endif