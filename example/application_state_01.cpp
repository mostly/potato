#include "application_state_01.hpp"

ApplicationState01::ApplicationState01(SDL_Renderer* screenRenderer)
{
    m_b_successfulInit = init(screenRenderer);
}

ApplicationState01::~ApplicationState01()
{
    delete m_p_previewImage;
}

std::string ApplicationState01::get_init_message()
{
	return m_s_initMessage;
}

bool ApplicationState01::get_successful_init_flag()
{
	return m_b_successfulInit;
}

void ApplicationState01::handle_events()
{
}

bool ApplicationState01::init(SDL_Renderer* screenRenderer)
{
	const std::string cs_testImagePath = "../example/resources/images/development/preview.png";
	m_p_previewImage = new potato::Texture();
	if (!(m_p_previewImage->load_from_file(cs_testImagePath.c_str(), screenRenderer)))
	{
		m_s_initMessage = m_p_previewImage->get_error_message();
		return false;
	}
	
	m_b_firstTimeThrough = true;

	return true;
}

std::string ApplicationState01::logic()
{
	if (m_b_firstTimeThrough)
	{
		m_b_firstTimeThrough = false;
		return STATE_NULL;	
	}
	else
	{
		SDL_Delay(2000);
		return STATE_02;
	}
	
}

void ApplicationState01::render(SDL_Renderer* screenRenderer)
{
	// Render texture to screen
	m_p_previewImage->render_stretched(0, 0, screenRenderer);
}

void ApplicationState01::reset()
{

}
