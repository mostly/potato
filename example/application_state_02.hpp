#ifndef APPLICATION_STATE_02_HPP
#define APPLICATION_STATE_02_HPP

#include "../potato/potato.hpp"

#include "application_states.hpp"

class ApplicationState02 : public potato::State
{
	private:
		bool             m_b_quitPressed;
		bool             m_b_successfulInit;
		potato::Texture* m_p_previewImage;
		std::string      m_s_initMessage;

    public:
		ApplicationState02(SDL_Renderer* screenRenderer);
    	~ApplicationState02();
		std::string get_init_message();
		bool        get_successful_init_flag();
		void        handle_events();
		std::string logic();
		bool        init(SDL_Renderer* screenRenderer);
    	void        render(SDL_Renderer* screenRenderer);
		void        reset();
};

#endif