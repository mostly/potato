#ifndef LOGGER_HPP
#define LOGGER_HPP

#include "log.hpp"

extern logging::logger< logging::file_log_policy > log_inst;


#define LOG_DEBUG log_inst.print< logging::severity_type::debug1 >
#define LOG_DEBUG2 log_inst.print< logging::severity_type::debug2 >
#define LOG_DEBUG3 log_inst.print< logging::severity_type::debug3 >
#define LOG_ERROR log_inst.print< logging::severity_type::error >
#define LOG_WARNING log_inst.print< logging::severity_type::warning1 >
#define LOG_WARNING2 log_inst.print< logging::severity_type::warning2 >

#define SET_LOG_THREAD_NAME(name) log_inst.set_thread_name(name);

#define LOGGING 1

#endif