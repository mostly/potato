#include "application_state_03.hpp"

ApplicationState03::ApplicationState03(int screenWidth, int screenHeight)
{
    m_b_successfulInit = init(screenWidth, screenHeight);
}

ApplicationState03::~ApplicationState03()
{
}

std::string ApplicationState03::get_init_message()
{
	return m_s_initMessage;
}

bool ApplicationState03::get_successful_init_flag()
{
	return m_b_successfulInit;
}

void ApplicationState03::handle_events()
{
	SDL_Event eventQueue; 
	// Poll the event queue
	while (SDL_PollEvent(&eventQueue) != 0)
	{
		switch (eventQueue.type)
		{
			case SDL_QUIT: // User requests quit
				m_b_quitPressed = true;
				break;
			case SDL_KEYDOWN: // User presses a key
				switch (eventQueue.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						m_b_quitPressed = true;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
}

bool ApplicationState03::init(int screenWidth, int screenHeight)
{
	m_b_quitPressed = false;
	m_i_screenWidth = screenWidth;
	m_i_screenHeight = screenHeight;

	return true;
}

std::string ApplicationState03::logic()
{
	if (m_b_quitPressed)
	{
		return STATE_04;	
	}
	else
	{
		return STATE_NULL;
	}
	
}

void ApplicationState03::render(SDL_Renderer* screenRenderer)
{
	// Red filled quad
	SDL_Rect fillRect = {m_i_screenWidth / 4, m_i_screenHeight / 4,
	                     m_i_screenWidth / 2, m_i_screenHeight / 2};
    SDL_SetRenderDrawColor(screenRenderer, 0xFF, 0x00, 0x00, 0xFF);        
    SDL_RenderFillRect(screenRenderer, &fillRect);

	// Green outlined quad
	SDL_Rect outlineRect = {m_i_screenWidth / 6, m_i_screenHeight / 6, 
	                        m_i_screenWidth * 2 / 3, m_i_screenHeight * 2 / 3};
	SDL_SetRenderDrawColor(screenRenderer, 0x00, 0xFF, 0x00, 0xFF);        
	SDL_RenderDrawRect(screenRenderer, &outlineRect);

	// Blue horizontal line
	SDL_SetRenderDrawColor(screenRenderer, 0x00, 0x00, 0xFF, 0xFF);
	SDL_RenderDrawLine(screenRenderer, 0, m_i_screenHeight / 2, 
	                   m_i_screenWidth, m_i_screenHeight / 2 );

	// Vertical line of yellow dots
	SDL_SetRenderDrawColor(screenRenderer, 0xFF, 0xFF, 0x00, 0xFF);
	for (int i = 0; i < m_i_screenHeight; i += 4)
	{
		SDL_RenderDrawPoint(screenRenderer, m_i_screenWidth / 2, i);
	}

	// Set the clear screen colour back to white
	SDL_SetRenderDrawColor(screenRenderer, 0xFF, 0xFF, 0xFF, 0xFF); 
}

void ApplicationState03::reset()
{

}
