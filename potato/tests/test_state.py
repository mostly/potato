"""Test potato test state class and helper functions"""
import unittest

import potato

class TestStates(unittest.TestCase):
	def setUp(self):
		pass

	def tearDown(self):
		pass

	def test_constructor(self):
		c = potato.TestState()
		self.assertEqual(list(c.get_history_list()), ['constructor'])

	def test_methods(self):
		c = potato.TestState()
		c.handle_events()
		c.logic()
		c.reset()
		c.render()
		self.assertEqual(list(c.get_history_list()), 
		                 ['constructor', 'handle_events', 'logic', 
						  'reset', 'render'])
