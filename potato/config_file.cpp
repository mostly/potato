#include "potato.hpp"
#include <iostream>

potato::ConfigFile::ConfigFile(std::string path)
{
	m_b_errorStatusFlag = false;
	m_s_errorMessage = "";
	tinyxml2::XMLError result = m_o_xmlDoc.LoadFile(path.c_str()); 
	if (result != tinyxml2::XML_SUCCESS)
	{
		m_s_errorMessage = "Failed to load config [" + path + "]." 
		                  + "Error = " + std::to_string(result);
		m_b_errorStatusFlag = true;
	}
	else
	{
		m_p_root = m_o_xmlDoc.FirstChild();
		if (m_p_root == nullptr)
		{
			m_s_errorMessage = "No root node found in config [" + path + "].";
			m_b_errorStatusFlag = true;
		}
	}
}

potato::Clipsheet* potato::ConfigFile::get_clipsheet(void)
{
	if (in_error_state())
	{
		return nullptr;
	}
	
	tinyxml2::XMLElement* element = m_p_root->FirstChildElement("Clipsheet");
	potato::Clipsheet* clipsheet = new potato::Clipsheet(element);
	if (!(clipsheet->get_successful_init_flag()))
	{
		m_s_errorMessage = clipsheet->get_init_message();
		m_b_errorStatusFlag = true;
	}

	return clipsheet;
}

std::string potato::ConfigFile::get_error_message(void)
{
	return m_s_errorMessage;
}

potato::Texture* potato::ConfigFile::get_texture(SDL_Renderer* renderer)
{
	if (in_error_state())
	{
		return nullptr;
	}

	tinyxml2::XMLElement* element = m_p_root->FirstChildElement("Texture");
	potato::Texture* texture = new potato::Texture();
	std::string path = element->Attribute("Path");
	if  (in_error_state())
	{
		return nullptr;
	}

	if (!(texture->load_from_file(path, renderer)))
	{
		m_s_errorMessage = texture->get_error_message();
		m_b_errorStatusFlag = true;
	}

	return texture;
}

bool potato::ConfigFile::in_error_state(void)
{
	return m_b_errorStatusFlag;
}