/* File : potato.i */
%module potato
%{
#include "potato.hpp"
%}

%include "std_vector.i"
%include "std_string.i"

%template(StringVector) std::vector<std::string>;

%include "potato.hpp"
