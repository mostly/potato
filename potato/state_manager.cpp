#include "potato.hpp"

potato::StateManager::StateManager()
{
	m_p_currentState = nullptr;
	m_s_nextState = "";
	m_m_states["STATE_NULL"] = nullptr;
	m_s_currentStateId = "STATE_NULL";
}

potato::StateManager::~StateManager()
{
	std::map<std::string, potato::State*>::iterator it;
	
	for (it = m_m_states.begin(); it != m_m_states.end(); it++)
	{
		delete it->second;
	}
}

bool potato::StateManager::register_state(std::string id, potato::State* state)
{
	if (m_m_states.find(id) != m_m_states.end())
	{
		// Cannot add state because this id exists in the map
		return false;
	}
	else 
	{
		// Add state
		m_m_states[id] = state;
		return true;
	}
}

void potato::StateManager::change_state()
{
    // If the state needs to be changed
    if (m_s_nextState != "STATE_NULL")
    {
		// Reset the current state
		if (m_s_currentStateId != ("STATE_NULL"))
		{
			m_p_currentState->reset();	
		}

        // Change the state
		m_p_currentState = m_m_states[m_s_nextState];

		// Update the id
		m_s_currentStateId = m_s_nextState;

        // NULL the next state ID
        m_s_nextState = "STATE_NULL";
    }
}

bool potato::StateManager::set_next_state(std::string id)
{
    if (m_m_states.find(id) != m_m_states.end())
	{
		// Add state
		m_s_nextState = id;
		return true;
	}
	else 
	{
		// Cannot set state because is not in map
		return false;
	}
}

std::string potato::StateManager::get_current_state_id()
{
	return m_s_currentStateId;
}

potato::State* potato::StateManager::get_current_state()
{
	return m_p_currentState;
}

potato_testing::TestState::TestState()
{
	m_v_history.push_back("constructor");
}

potato_testing::TestState::~TestState()
{
	m_v_history.push_back("destructor");
}

std::vector<std::string> potato_testing::TestState::get_history_list()
{
	return m_v_history;
}

void potato_testing::TestState::handle_events()
{
	m_v_history.push_back("handle_events");
}

std::string potato_testing::TestState::logic()
{
	m_v_history.push_back("logic");
	std::string returnValue;
	return returnValue;
}

void potato_testing::TestState::render(SDL_Renderer* screenRenderer)
{
	m_v_history.push_back("render");
}

void potato_testing::TestState::reset()
{
	m_v_history.push_back("reset");	
}
