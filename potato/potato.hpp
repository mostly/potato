#ifndef POTATO_HPP
#define POTATO_HPP

#include <map>
#include <string>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "tinyxml2.hpp"

namespace potato
{
	class Clipsheet
	{
		private:
			bool                             m_b_successfulInit;
			std::map<std::string, SDL_Rect*> m_m_clips;
			std::string                      m_s_initMessage;

		public:
			Clipsheet(tinyxml2::XMLElement* pElement);
			~Clipsheet();
			SDL_Rect*   get(std::string id);
			std::string get_init_message();
			bool        get_successful_init_flag();
			bool        init(tinyxml2::XMLElement* pElement);
	};

	class State
	{
		public:
			virtual ~State(){};
			virtual void        handle_events() = 0;
			virtual std::string logic() = 0;
			virtual void        render(SDL_Renderer* screenRenderer) = 0;
			virtual void        reset() = 0;
	};

	class StateManager
	{
		private:
		    State*                        m_p_currentState;
			std::map<std::string, State*> m_m_states;
			std::string                   m_s_nextState;
			std::string                   m_s_currentStateId;

		public:
			StateManager();
			~StateManager();
			void        change_state();
			State*      get_current_state();
			std::string get_current_state_id();
			bool        register_state(std::string id, State* state);
			bool        set_next_state(std::string stateId);
	};
	
	class Texture
	{
		private:
			int          m_i_height;
			int          m_i_width;
			std::string  m_s_errorMessage;
			SDL_Texture* m_p_texture;

		public:
			Texture(void);
			~Texture(void);
			void        free(void);
			std::string get_error_message(void);
			int         get_height(void);
			int         get_width(void);
			bool        load_from_file(std::string path, SDL_Renderer* renderer);
			void        render(int x, int y, SDL_Renderer* renderer, 
			                   SDL_Rect* clip = nullptr);
			void        render_stretched(int x, int y, SDL_Renderer* renderer);
			void        set_alpha(Uint8 alpha);
			void        set_blend_mode(SDL_BlendMode blending);
            void        set_colour(Uint8 red, Uint8 green, Uint8 blue);
	};

	class ConfigFile
	{
		private:
			bool                  m_b_errorStatusFlag;
			std::string           m_s_errorMessage;
			tinyxml2::XMLDocument m_o_xmlDoc;
			tinyxml2::XMLNode*    m_p_root;

		public:
			ConfigFile(std::string path);
			Clipsheet*  get_clipsheet(void);
			std::string get_error_message(void);
			Texture*    get_texture(SDL_Renderer* renderer);
			bool        in_error_state(void);
	};
}


namespace potato_testing
{
	class TestState : private potato::State
	{
		private:
			std::vector<std::string> m_v_history;

		public:
			TestState();
			~TestState();
			std::vector<std::string> get_history_list();
			void                     handle_events();
			std::string              logic();
			void                     render(SDL_Renderer* screenRenderer=nullptr);
			void                     reset();
	};
}

#endif