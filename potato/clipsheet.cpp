#include "potato.hpp"

potato::Clipsheet::Clipsheet(tinyxml2::XMLElement* pElement)
{
	m_s_initMessage = "";
	m_b_successfulInit = init(pElement);
}

potato::Clipsheet::~Clipsheet()
{
	std::map<std::string, SDL_Rect*>::iterator it;
	
	for (it = m_m_clips.begin(); it != m_m_clips.end(); it++)
	{
		delete it->second;
	}
}

bool potato::Clipsheet::init(tinyxml2::XMLElement* pElement)
{
	if (pElement == nullptr) 
	{
		m_s_initMessage = "Element pointer pointing to NULL value.";
		return false;
	}

	tinyxml2::XMLElement * pListElement = pElement->FirstChildElement("Clip");
	while (pListElement != nullptr)
	{
		// Get the map key
		const char * mapKeyRead = nullptr;
		mapKeyRead = pListElement->Attribute("Key");
        if (mapKeyRead == nullptr) 
		{
			m_s_initMessage = "Unable to read Key attribute.";
			return false;
		}
		std::string mapKey = mapKeyRead;

		// Get the x value
		const char * xValueRead;
		xValueRead = pListElement->Attribute("X");
		if (xValueRead == nullptr) 
		{
			m_s_initMessage = "Unable to read X attribute for Key=" + mapKey;
			return false;
		}

		// Get the y value
		const char * yValueRead;
		yValueRead = pListElement->Attribute("Y");
		if (yValueRead == nullptr) 
		{
			m_s_initMessage = "Unable to read Y attribute for Key=" + mapKey;
			return false;
		}

		// Get the w value
		const char * wValueRead;
		wValueRead = pListElement->Attribute("W");
		if (wValueRead == nullptr) 
		{
			m_s_initMessage = "Unable to read W attribute for Key=" + mapKey;
			return false;
		}

		// Get the h value
		const char * hValueRead;
		hValueRead = pListElement->Attribute("H");
		if (hValueRead == nullptr) 
		{
			m_s_initMessage = "Unable to read H attribute for Key=" + mapKey;
			return false;
		}

		// Check if the key is already used
		if (m_m_clips.find(mapKey) != m_m_clips.end())
		{
			// Cannot add clip because this key already exists in the map
			m_s_initMessage = "Duplicate entry in map for Key=" + mapKey;
			return false;
		}

		// Add the clip to the map
		m_m_clips[mapKey] = new SDL_Rect;
		m_m_clips[mapKey]->x = std::stoi(xValueRead);
		m_m_clips[mapKey]->y = std::stoi(yValueRead);
		m_m_clips[mapKey]->w = std::stoi(wValueRead);
		m_m_clips[mapKey]->h = std::stoi(hValueRead);
		
		// Iterate onto the next clip
		pListElement = pListElement->NextSiblingElement("Clip");
	}

	return true;
}

bool potato::Clipsheet::get_successful_init_flag()
{
	return m_b_successfulInit;
}

std::string potato::Clipsheet::get_init_message()
{
	return m_s_initMessage;
}

SDL_Rect* potato::Clipsheet::get(std::string id)
{
	if (m_m_clips.find(id) != m_m_clips.end())
	{
		return m_m_clips[id];
	}
	else 
	{
		return nullptr;
	}
}