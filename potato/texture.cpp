#include "potato.hpp"

potato::Texture::Texture()
{
    m_p_texture = nullptr;
	m_i_height = 0;
	m_i_width = 0;
	m_s_errorMessage = "";
}

potato::Texture::~Texture()
{
    free();
}

void potato::Texture::free()
{
    if (m_p_texture != nullptr)
    {
        SDL_DestroyTexture(m_p_texture);
        m_p_texture = nullptr;
		m_i_height = 0;
		m_i_width = 0;
    }
}

std::string potato::Texture::get_error_message()
{
	return m_s_errorMessage;
}

int potato::Texture::get_height(void)
{
	return m_i_height;
}

int potato::Texture::get_width(void)
{
	return m_i_width;
}

bool potato::Texture::load_from_file(std::string imageLocation, SDL_Renderer* renderer)
{
	// Free any pre-existing texture
	free();

	SDL_Texture* p_finalTexture = nullptr;

    SDL_Surface* p_loadedSurface = IMG_Load(imageLocation.c_str());
    if (p_loadedSurface == nullptr)
    {
		m_s_errorMessage = "Unable to load image [" + imageLocation 
		                   + "] SDL_image Error=" + IMG_GetError();
    }
	else
	{
		// Color key image
		SDL_SetColorKey(p_loadedSurface, SDL_TRUE, SDL_MapRGB(p_loadedSurface->format, 
		                0, 0xFF, 0xFF));

		// Create texture from surface pixels
        p_finalTexture = SDL_CreateTextureFromSurface(renderer, p_loadedSurface);
        if (p_finalTexture == nullptr)
        {
			m_s_errorMessage = "Unable to create texture from  [" + imageLocation 
		                       + "] SDL_image Error=" + IMG_GetError();
        }
		else
		{
			// Get image dimensions
			m_i_width = p_loadedSurface->w;
			m_i_height = p_loadedSurface->h;
		}

		// Get rid of old loaded surface
		SDL_FreeSurface(p_loadedSurface);
	}

	m_p_texture = p_finalTexture;
	return m_p_texture != nullptr;
}

void potato::Texture::render(int x, int y, SDL_Renderer* renderer, SDL_Rect* clip)
{
	SDL_Rect renderQuad = {x, y, m_i_width, m_i_height};

 	// Set clip rendering dimensions
    if (clip != nullptr)
    {
        renderQuad.w = clip->w;
        renderQuad.h = clip->h;
    }

	SDL_RenderCopy(renderer, m_p_texture, clip, &renderQuad);
}

void potato::Texture::render_stretched(int x, int y, SDL_Renderer* renderer)
{
	SDL_RenderCopy(renderer, m_p_texture, nullptr, nullptr);
}

void potato::Texture::set_alpha(Uint8 alpha)
{
	SDL_SetTextureAlphaMod(m_p_texture, alpha);
}

void potato::Texture::set_blend_mode(SDL_BlendMode blending)
{
	SDL_SetTextureBlendMode(m_p_texture, blending);
}

void potato::Texture::set_colour(Uint8 red, Uint8 green, Uint8 blue)
{
    SDL_SetTextureColorMod(m_p_texture, red, green, blue);
}